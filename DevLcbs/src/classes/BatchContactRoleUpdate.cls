global class BatchContactRoleUpdate implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id,Name,EnvironmentalAccountType__c,EnvironmentalContractorProNumber__c FROM Account where  EnvironmentalAccountType__c = \'Contractor\' and EnvironmentalContractorProNumber__c != null and ID IN (Select AccountId from contact where ContractorProContact__c = true)';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Account> scope) {
        system.debug('Scope : '+Scope);
        List<Contact> updatedContacts = new List<Contact>();
        Set<Id> Accids = new Set<id>();
        for(Account a :  Scope){
            Accids.add(a.id);     
        }
     
        List<contact> contacts = [SELECT id,Role__c,accountId FROM contact WHERE accountId in : Accids];
        for(Account a : scope){
            for (Contact c : contacts){
                c.Role__c =  'owner'; 
                updatedContacts.add(c); 
            }          
        }
        
        update updatedContacts;
    }   
    
    global void finish(Database.BatchableContext BC) { }
}