@isTest(SeeAllData=false)
public class LCBSAlertNotificationRestAPI_test
{

static testMethod void LCBSAlertNotificationRestAPI_test()
{
LCBSAlertNotificationRestAPI api = new LCBSAlertNotificationRestAPI();
LCBSAlertNotificationRestAPI.TransactionId trans = new LCBSAlertNotificationRestAPI.TransactionId('a0Eg0000003HT9H');
LCBSAlertNotificationRestAPI.NotificationType a = new LCBSAlertNotificationRestAPI.NotificationType('test');
LCBSAlertNotificationRestAPI.EmailStatus b = new LCBSAlertNotificationRestAPI.EmailStatus('test','test');
LCBSAlertNotificationRestAPI.AlertNotificationResponse c = new LCBSAlertNotificationRestAPI.AlertNotificationResponse('test','test','test','test');

//api.AlertNotification();


}

static testMethod void testWebService(){
  Profile randomProfile = [SELECT Id FROM Profile WHERE UserType = 'CspLitePortal' LIMIT 1];
        //create accounts for our portal users
        List<account> partnerAccounts = new List<account>();
        partnerAccounts.add(new Account(Name = 'Testing Communities Company'));
        insert partnerAccounts;
        //Create some Contacts because we want to create portal users we are required to provide a corresponding contactId
        List<contact> partnerContacts = new List<contact>();
        partnerContacts.add(new Contact(AccountId = partnerAccounts[0].Id, Email = 'communitiesupdateduser@interactiveties.com', FirstName = 'Demo1', LastName = 'User1',High_Priority__c ='Email'));
        insert partnerContacts;
        List<user> newUsers = new List<user>();
        newUsers.add(new User(Alias = 'test01', ContactId = partnerContacts[0].Id, Email = 'communitiesupdateduser@interactiveties.com', EmailEncodingKey = 'ISO-8859-1', FirstName = 'Demo1', IsActive = true, LanguageLocaleKey = 'en_US', LastName = 'User1', LocaleSidKey = 'en_US', MobilePhone = '(303) 555-0000', Phone = '(303) 555-2222', ProfileId = randomProfile.Id, TimeZoneSidKey = 'America/New_York', Username = 'communitiesupdateduser@interactiveties.com'));
        insert newUsers;
        //END: some setup items
        //validate the initial contact information
        Contact c = [SELECT Email, FirstName, LastName, MailingCity, MailingCountry,High_Priority__c , MailingPostalCode, MailingState, MailingStreet, Phone, Title FROM Contact WHERE Id =: partnerContacts[0].Id];
        System.assertEquals('communitiesupdateduser@interactiveties.com', c.Email);
        System.assertEquals('Demo1', c.FirstName);
        System.assertEquals('User1', c.LastName);
        System.assertEquals('Email', c.High_Priority__c );
        System.assertEquals(null, c.MailingCity);
        System.assertEquals(null, c.MailingCountry);
        System.assertEquals(null, c.MailingPostalCode);
        System.assertEquals(null, c.MailingState);
        System.assertEquals(null, c.MailingStreet);
        System.assertEquals(null, c.Phone);
        System.assertEquals(null, c.Title);

    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    
    req.requestURI = '/services/apexrest/AlertNotification';  
    req.httpMethod = 'POST';
    RestContext.request = req;
    RestContext.response= res;

 
    JSON2Apex a = new JSON2Apex();
    JSON2Apex.AlertNotificationRequest s=new JSON2Apex.AlertNotificationRequest();
    LCBSAlertNotificationRestAPI.AlertNotification();
    
    
}



}