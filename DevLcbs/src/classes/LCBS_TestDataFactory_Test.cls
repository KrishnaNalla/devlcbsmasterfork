@isTest
public class LCBS_TestDataFactory_Test {

    static Integer count = Integer.valueof(Math.random()*10);
    static testmethod void TestCreateAccounts(){

        List<Account> acclst = new List<Account>();
        acclst = LCBS_TestDataFactory.createAccounts(count);
        System.assertEquals(acclst.size(),count);
    }
    
    static testmethod void TestCreateContacts(){
    
        List<Contact> conlst = new List<Contact>();
        conlst = LCBS_TestDataFactory.createContact(count);
        System.assertEquals(conlst.size(),count);
        conlst = LCBS_TestDataFactory.createContactDummy(count);
        System.assertEquals(conlst.size(),count);
    }
    
    static testmethod void TestcreatecompanyLocation(){
        
        List<Company_Location__c> comploclst = new List<Company_Location__c>();
        comploclst = LCBS_TestDataFactory.createcompanyLocation(count);
        System.assertEquals(comploclst.size(),count);
    }
    
    static testmethod void TestcreateBuildingOwner(){
    
        List<Building_Owner__c> bolst = new List<Building_Owner__c>();
        bolst = LCBS_TestDataFactory.createBuildingOwner(count);
        System.assertEquals(bolst.size(),count);
    }
    
    static testmethod void TestcreateBuilding(){
         List<Building__c> bldlst = new  List<Building__c>();
         bldlst = LCBS_TestDataFactory.createBuilding(count);
         System.assertEquals(bldlst.size(),count);
    }
}