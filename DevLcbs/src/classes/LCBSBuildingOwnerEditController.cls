/************************************
Name         : LCBSBuildingOwnerEditController
Created By   : Chiranjeevi Gogulakonda
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  :
***********************************/

public with sharing class LCBSBuildingOwnerEditController {
    
    public boolean BudOwnrPanel { get; set; } 
    public boolean BudEditOwnrPanel{ get; set; }
    public boolean MainPanel {get; set;}
    public boolean EditPanel {get; set;}
    public Building_Owner__c BuldEdit{set;get;}
    public boolean isDistributor {get;set;}
    public String PAGE_CSS { get; set; }


    Id BuldId;
    public LCBSBuildingOwnerEditController(){ 
        MainPanel =true;
        EditPanel = false;   
        BudOwnrPanel = true; 
        BudEditOwnrPanel = false;  
        BuldId = apexpages.currentpage().getparameters().get('id') ;                    
        BuldEdit =[Select id,Name,Address__c,Address_1__c,Address_2__c,City__c,Contractor_Branch_Location__c,Company_Company_Location12__r.name,Company_Company_Location12__c,Country__c,Email_Address__c,First_Name__c,Last_Name__c,Phone_Number__c,Postal_Code__c,State__c,Time_Zone__c from Building_Owner__c where id =: BuldId];   
        //BuldEdit.Contractor_Company_Location__c = 'Acme Heating & Cooling';  
        User u = [select id,Contactid,AccountId,Account.EnvironmentalAccountType__c from User where id=:userinfo.getuserId()];
         if(u.Account.EnvironmentalAccountType__c == 'Distributor')
          {
           isDistributor = true;
          }
          else if(u.Account.EnvironmentalAccountType__c == 'Contractor')
          {
           isDistributor = false;
          }   
          PAGE_CSS = LCBSParameters.PAGE_CSS_URL;       
    }   
    
    public SelectOption[] getContractorCompanyLoc() 
    {            
            SelectOption[] getContractorCompanyLocNames= new SelectOption[]{};  
                getContractorCompanyLocNames.add(new SelectOption('','--None--'));
            for (Company_Location__c co : [select id,Name from Company_Location__c order by name]) 
            {  
                getContractorCompanyLocNames.add(new SelectOption(co.id,co.Name));   
                
            }
            return getContractorCompanyLocNames;               
    } 
    
    
    public PageReference buidingCancel() {
        PageReference p = new PageReference('/apex/LCBSBuildingOwners');
        p.setredirect(true);
        return p;
       
    }   
    public PageReference buidingNewSave() {   
        try{        
            update BuldEdit;
        }
        catch(DmlException e){  String error = e.getMessage(); ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));  }                     
        system.debug('Test@@@@@@'+BuldEdit);
        //Id id = System.currentPageReference().getParameters().get('id');
        PageReference p = new PageReference('/apex/LCBSBuildingOwners'); //LCBSNewBuildingOwner?id='+id);
        p.setredirect(true);
        return p;       
    }  
    public PageReference ownerEdit() {       
        MainPanel = False;
        EditPanel = True;
        BudOwnrPanel = False; 
        BudEditOwnrPanel = True;  
        return null;
    }   
     public PageReference ownerDelete() {       
        Building_Owner__c b = [select id from Building_Owner__c where id=:BuldId limit 1];
        Delete b;
        PageReference p = new PageReference('/apex/LCBSBuildingOwners'); //LCBSNewBuildingOwner?id='+id);
        p.setredirect(true);
        return p; 
        
    }   
}